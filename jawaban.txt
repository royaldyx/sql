Yoga Pratama Royaldy


Soal 1 Membuat Database
Buatlah database dengan nama “myshop”.

Jawaban :  create database myshop;

Soal 2 Membuat Table di Dalam Database

Jawaban : 
(users) create table users(
		-> id int primary key auto_increment,
		-> name varchar(255),
		-> email varchar(255),
		-> password varchar(255)
		-> );

(categories) create table categories(
 			 -> id int primary key auto_increment,
   			 -> name varchar(255)
   			 -> );

(items) create table items(
    	-> id int primary key auto_increment,
    	-> name varchar(255),
    	-> description varchar(255),
    	-> price int,
    	-> stock int
    	-> );
penggabung :  alter table items add foreign key (category_id) references categories(id);


Soal 3 Memasukkan Data pada Table

(users) insert into users(name, email, password) Values(
    	-> "John Doe", "john@doe.com", "john123"), (
    	-> "Jane Doe", "jane@doe.com", "jenita123");

(categories) insert into categories(name) value("gadget"),("cloth"),("men"),("women"),("branded");

(items) insert into items(name, description, price, stock, category_id) Value(
   		-> "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), (
   		-> "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), (
   		-> "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

Soal 4 Mengambil Data dari Database
a. Mengambil data users
jawaban : select id, name, email from users;

b. Mengambil data items
(items yang memiliki harga di atas 1000000 (satu juta))
jawaban : select * from items where price > 1000000; 

items yang memiliki name serupa atau mirip (like) dengan kata kunci “watch”
jawaban : select * from items
		  -> where name LIKE '%watch%'
   		  -> ;

c. Menampilkan data items join dengan kategori
items yang dilengkapi dengan data nama kategori di masing-masing items.
jawaban : select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
jawaban : update items set price = 2500000 where id=1;